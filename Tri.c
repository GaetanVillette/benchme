/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdbool.h>
#include <time.h>

/*
* @param tab = tableau à trier
* @param size_tab = taille du tableau en param d'entrée
* @return rien car tab est utilisé avec malloc
*/
void Tri_Bulle_Croissant(float tab[], int size_tab)
{
    bool flag = true;
    while ( flag) {
        flag = false;
        for (int i = 0;i < size_tab-1; i++) {
            if (tab[i]>tab[i+1]){
                //swap + fin de boucle
                float var_temp;
                var_temp= tab[i];
                tab[i] = tab[i+1];
                tab[i+1] = var_temp;
                flag = true;
            }
        }
    }
}

/*
* @param tab = tableau à trier
* @param size_tab = taille du tableau en param d'entrée
* @return rien car tab est utilisé avec malloc
*/
void Tri_Bulle_Decroissant(float* tab,int size_tab)
{
    bool flag = true;
    while ( flag) {
        flag = false;
        for (int i = 0;i < size_tab-1; i++) {
            if (tab[i]<tab[i+1]){
                //swap+fin de boucle
                float var_temp = tab[i];
                tab[i] = tab[i+1];
                tab[i+1] = var_temp;
                flag = true;
            }
        }
    }
}

/*
* @param tab = tableau à trier
* @param size_tab = taille du tableau en param d'entrée
* @return rien car tab est utilisé avec malloc
*/
void Tri_Selection_Croissant(float *tab,int size_tab)
{
     for (int i = 0; i < size_tab - 1; i++){
         int min = i;
         for (int j = i; j < size_tab; j++){
            if (tab[j] < tab[min]){
                min = j; 
            }
         }
         //swap
          float var_temp = tab[i];
          tab[i] = tab[min];
          tab[min] = var_temp;
     }
}

/*
* @param tab = tableau à trier
* @param size_tab = taille du tableau en param d'entrée
* @return rien car tab est utilisé avec malloc
*/
void Tri_Selection_Decroissant(float *tab,int size_tab)
{
     for (int i = 0; i < size_tab - 1; i++){
         int max = i;
         for (int j = i; j < size_tab; j++){
            if (tab[j] > tab[max]){
                max = j; 
            }
         }
         //swap
          float var_temp = tab[i];
          tab[i] = tab[max];
          tab[max] = var_temp;
     }
}

/*
* @param tab = tableau à trier
* @param size_tab = taille du tableau en param d'entrée
* @return rien car tab est utilisé avec malloc
*/
void Tri_Insertion_Croissant(float* tab, int size_tab)
{
    for (int i = 1; i < size_tab-1; i++) {
        float var_temp = tab[i];
        for (int j = i; tab[j - 1] > var_temp; j--) {
            tab[j] = tab[j - 1];
            tab[j] = var_temp;
        }
    }
}

/*
* @param tab = tableau à trier
* @param size_tab = taille du tableau en param d'entrée
* @return rien car tab est utilisé avec malloc
*/
void Tri_Insertion_Decroissant(float* tab, int size_tab)
{
    for (int i = 1; i < size_tab-1; i++) {
        float var_temp = tab[i];
        for (int j = i; tab[j-1] < var_temp; j--) {
            tab[j] = tab[j - 1];
            tab[j] = var_temp;
        }
    }
}