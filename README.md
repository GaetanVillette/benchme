Dans le main.c:

On déclare les attributs pour générer un tableau de nombre pseudo aléatoires avec la fonction rand().

Ensuite pour le benchmark, on crée des variables temporaires pour stocker le temps d'éxécution de chaque fonction de tri.

On lance chaque fonction de tri 3x afin de faire une moyenne des temps d'éxécution de chaque fonction.