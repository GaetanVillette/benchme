#include <stdio.h>

int main(int argc, char *argv[]) {
	printf("Il y a %d arguments\n",argc);
	for (int i=0; i < argc; i++)
		printf("Liste des arguments : %s\n", argv[i]);

	double timeSC = 2;
	double timeIC = 6;
	double timeBC = 5;

	FILE *Efichier = fopen(argv[1],"w");
	fprintf(Efichier, "%lf;%lf;%lf", timeSC, timeIC, timeBC);

	fclose(Efichier);

	double FiletimeSC;
	double FiletimeIC;
	double FiletimeBC;

	FILE *Rfichier = fopen(argv[1],"r");
	fscanf(Rfichier, "%lf;%lf;%lf", &FiletimeSC, &FiletimeIC, &FiletimeBC);
	printf("Temps selection est égale à %lf secondes\nTemps insertion est égale à %lf secondes\nTemps bulle est égale à %lf secondes", FiletimeSC, FiletimeIC, FiletimeBC);

	fclose(Rfichier);
	return 0;
}
