/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Tri.h
 * Author: student
 *
 * Created on September 26, 2019, 8:22 AM
 */

#ifndef TRI_H
#define TRI_H

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

void Tri_Bulle_Croissant(float* tab, int size_tab);
void Tri_Bulle_Decroissant(float* tab, int size_tab);

void Tri_Selection_Croissant(float *tab, int size_tab);
void Tri_Selection_Decroissant(float *tab, int size_tab);

void Tri_Insertion_Croissant(float* tab, int size_tab);
void Tri_Insertion_Decroissant(float* tab, int size_tab);

#endif /* TRI_H */

