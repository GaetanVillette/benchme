/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: student
 *
 * Created on September 26, 2019, 9:07 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "Tri.h"

/*
 * 
 */
int main(/*int argc, char** argv*/void) {

  int taille_tableau=999;
  float *tab;
  tab = malloc(sizeof(float) * taille_tableau);
  //Déclaration des variables pour le benchmark
  clock_t start, end;
  double timeIC, timeBC, timeSC;
  int j=0;

   //Tri_Bulle_Croissant(tab,taille_tableau);
   //Tri_Bulle_Decroissant(tab,taille_tableau);
   //Tri_Selection_Croissant(tab,taille_tableau);
   //Tri_Selection_Decroissant(tab,taille_tableau);
   //Tri_Insertion_Croissant(tab, taille_tableau);
  
  //Calcul du temps moyen d'éxécution de chaque fonction de tri
  for (int i=0; i<3; i++)
  {
      for(int i=0; i<taille_tableau; i++)
      {
         j=rand()%100000000;
         tab[i]=j+1;
      }
     start = clock();
     Tri_Bulle_Croissant(tab, taille_tableau);
     end = clock();
     timeBC = timeBC +((double)(end - start) / (double)CLOCKS_PER_SEC);
     for (int i = 0; i < taille_tableau; i++) {
        //printf("Liste des valeurs du tableau bulle= %.0f\n", tab[i]);
    }
  }
  
  for (int i =0; i<3; i++)
  {
      for(int i=0; i<taille_tableau; i++)
      {
         j=rand()%100000000;
         tab[i]=j+1;
      }
     start = clock();
     Tri_Selection_Croissant(tab, taille_tableau);
     end = clock();
     timeSC = timeSC + ((double)(end - start) / (double)CLOCKS_PER_SEC);
     for (int i = 0; i < taille_tableau; i++) {
    //printf("Liste des valeurs du tableau selection = %.0f\n", tab[i]);
  }
  }
  
  for (int i =0; i < 3; i++)
  {
      for(int i=0; i<taille_tableau; i++)
      {
         j=rand()%100000000;
         tab[i]=j+1;
      }
     start = clock();
     Tri_Insertion_Croissant(tab, taille_tableau);
     end = clock();
     timeIC = timeIC + ((double)(end - start) / (double)CLOCKS_PER_SEC);
     for (int i = 0; i < taille_tableau; i++) {
    //printf("Liste des valeurs du tableau insertion = %.0f\n", tab[i]);
    }
   }
    /*start = clock();
    Tri_Bulle_Decroissant(tab, taille_tableau);
    end = clock();
    timeBD = timeBD + (end - start) / (double)CLOCKS_PER_SEC;*/
    
    /*start = clock();
    Tri_Selection_Decroissant(tab, taille_tableau);
    end = clock();
    timeSD = timeSD + (end - start) / (double)CLOCKS_PER_SEC;*/
    
    /*start = clock();
    Tri_Insertion_Decroissant(tab, taille_tableau);
    end = clock();
    timeIC = timeIC + (end - start) / (double)CLOCKS_PER_SEC;*/
  
  printf("Temps moyen d'execution tri bulle croissant: %lf\n", timeBC);
  printf("Temps moyen d'execution tri selection croissant: %lf\n",timeSC);
  printf("Temps moyen d'execution tri insertion croissant: %lf\n", timeIC);
  
  FILE *Efichier = fopen(argv[1],"w");
  fprintf(Efichier, "%lf;%lf;%lf", timeSC/3, timeIC/3, timeBC/3);

  fclose(Efichier);


  double FiletimeSC;
	double FiletimeIC;
	double FiletimeBC;

  FILE *Rfichier = fopen(argv[1],"r");
	fscanf(Rfichier, "%lf;%lf;%lf", &FiletimeSC, &FiletimeIC, &FiletimeBC);
	printf("Temps selection est égale à %lf secondes\nTemps insertion est égale à %lf secondes\nTemps bulle est égale à %lf secondes\n", FiletimeSC, FiletimeIC, FiletimeBC);

	fclose(Rfichier);
    /*
  for (int i = 0; i < taille_tableau; i++) {
    printf("Liste des valeurs du tableau = %.0f\n", tab[i]);
  }
     */
   free(tab);
  return 0;
  
}

